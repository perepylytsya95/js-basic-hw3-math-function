let userNumber1;
do{
    userNumber1 = +prompt("Enter your first number");
} while (Number.isNaN(userNumber1));

let userNumber2;
do{
    userNumber2 = +prompt("Enter your second number");
} while (Number.isNaN(userNumber2));


let userMathOperation;
do{
    userMathOperation = prompt(`Choose and enter math operation you want to use:
+
-
*
/`)
} while (userMathOperation !== "+" && userMathOperation !== "-" && userMathOperation !== "*" && userMathOperation !== "/");

const returnCalculation = (a, b, c) =>{
    let result;
    if (c === "+"){
       result = a + b;
    } else if (c === "-"){
        result = a - b;
    } else if (c === "*"){
        result = a * b;
    } else if (c === "/"){
        result = a / b;
    }
    return result;
}

let userExpectedNum = returnCalculation(userNumber1, userNumber2, userMathOperation);
console.log(`${userNumber1} ${userMathOperation} ${userNumber2} equals ${userExpectedNum}`);